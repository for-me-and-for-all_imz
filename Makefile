# pdflatex (as opposed to latex + xdvi) has the problem that it doesn't force the generation of the missing fonts;
# this is why I add an option. -- https://bugzilla.altlinux.org/show_bug.cgi?id=24474
# (It's true at least for the old, "buggy" teTeX in ALTLinux; TeXLive might do better.)
PDFLATEX = pdflatex -mktex=pk

# The first goal is the default one:

scalimp_inference.pdf: scalimp_inference.tex
	$(PDFLATEX) $< \
	&& $(PDFLATEX) $< \
	&& $(PDFLATEX) $< 
	# latex, as usual, is run the 2nd time to get the refs correct, and the 3rd time to stabilize the page numbers.
